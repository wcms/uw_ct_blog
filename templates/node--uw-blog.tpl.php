<?php

/**
 * @file
 * Template for blog nodes.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <div class="blog-post-header">
      <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>

      <?php print $user_picture; ?>
      <?php if ($display_submitted): ?>
        <div class="submitted">
          <?php
            if (empty($node->field_blog_date['und'][0]['value'])) {
              $blog_date = $node->created;
            }
            else {
              $blog_date = strtotime($node->field_blog_date['und'][0]['value']);
            }
            if (!empty($node->field_author['und'][0]['title'])) {
                if (!empty($node->field_author['und'][0]['url'])) {?>
                  <span property="dc:date dc:created" content="<?php print format_date($node->created, 'custom', "c"); ?>" datatype="xsd:dateTime">
                  <?php print format_date($blog_date, 'custom', "l, F j, Y"); ?></span> <?php print t('by'); ?> <a rel="author" href="<?php print $node->field_author['und'][0]['url'];?>"><?php print $node->field_author['und'][0]['title'];?></a></div>
                <?php }
                else { ?>
                  <span property="dc:date dc:created" content="<?php print format_date($node->created, 'custom', "c"); ?>" datatype="xsd:dateTime">
                  <?php print format_date($blog_date, 'custom', "l, F j, Y"); ?></span> <?php print t('by'); ?> <?php print $node->field_author['und'][0]['title'];?></div>
                <?php }
            }
            else {
              // Get the realname of 'Authored by' under Authoring information
              // from the node displayed.
              $node_author = user_load($node->uid);
              $name = $node_author->realname;
            ?>
              <span property="dc:date dc:created" content="<?php print format_date($node->created, 'custom', "c"); ?>" datatype="xsd:dateTime">
              <?php print format_date($blog_date, 'custom', "l, F j, Y"); ?></span> <?php print t('by'); ?> <?php print $name; ?></div>
            <?php }
      endif; ?>
        </div>
        <div class="content_node"<?php print $content_attributes; ?>>
      <?php
        // Hide field_blog_date so it only shows in above
        // hide($content['field_blog_date']);
        // Hide field_author so it only shows in above.
        hide($content['field_author']);
        // Hide field_tag so we can render it after the "read more" link.
        hide($content['field_tag']);
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);

        if ($teaser) {
          // Filter and trim the teaser content.
          $content['body'][0]['#markup'] = uw_ct_web_page_filter_summary($content['body']['#items'][0]);
          $content['body'][0]['#markup'] = preg_replace("/<img[^>]+\>/i", "", $content['body'][0]['#markup']);
          hide($content['title_field']);
          hide($content['field_blog_date']);
        }
        hide($content['service_links']);
        print render($content);
       ?>
    </div>
    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>
    <?php if (!empty($content['field_tag'])): ?>
      <?php print render($content['field_tag']); ?>
    <?php endif; ?>
    <?php print render($content['service_links']); ?>
  </div> <!-- /node-inner -->
</div> <!-- /node-->
<?php print render($content['comments']); ?>
<?php if (!empty($content['links'])): ?>
  <div class="links"><?php print render($content['links']); ?></div>
<?php endif; ?>
