<?php

/**
 * @file
 * uw_ct_blog.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_blog_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_blog_post';
  $context->description = 'Displays Blog post recent, topics, category and archive block ';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog' => 'blog',
        'blog/*' => 'blog/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_blog-blog_public_feed' => array(
          'module' => 'uw_ct_blog',
          'delta' => 'blog_public_feed',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-uw_blog_recent-block_1' => array(
          'module' => 'views',
          'delta' => 'uw_blog_recent-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-uw_blog_topics-block_1' => array(
          'module' => 'views',
          'delta' => 'uw_blog_topics-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-5bbe76328202cacac13375a40dd59481' => array(
          'module' => 'views',
          'delta' => '5bbe76328202cacac13375a40dd59481',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'uw_ct_blog-blog_by_audience' => array(
          'module' => 'uw_ct_blog',
          'delta' => 'blog_by_audience',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'uw_ct_blog-blog_by_date' => array(
          'module' => 'uw_ct_blog',
          'delta' => 'blog_by_date',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays Blog post recent, topics, category and archive block ');
  $export['uw_blog_post'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_blog_post-front_page';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_blog-front_page' => array(
          'module' => 'uw_ct_blog',
          'delta' => 'front_page',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['uw_blog_post-front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_blog_post_related';
  $context->description = 'Displays Blog post related content';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog/post/*' => 'blog/post/*',
        'blog/archive/~' => 'blog/archive/~',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_blog_related_content-block' => array(
          'module' => 'views',
          'delta' => 'uw_blog_related_content-block',
          'region' => 'sidebar_second',
          'weight' => '-30',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays Blog post related content');
  $export['uw_blog_post_related'] = $context;

  return $export;
}
