<?php

/**
 * @file
 * uw_ct_blog.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_blog_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'uwaterloo_blog_v1';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/blog';
  $endpoint->authentication = array(
    'services_api_key_auth' => array(
      'api_key' => '96ab9383e6ad48c23aa1504dc9cc5c52',
      'api_key_source' => 'request',
      'user' => 'WCMS web service user',
    ),
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'all_blog_posts' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'single_blog_post' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['uwaterloo_blog_v1'] = $endpoint;

  return $export;
}
