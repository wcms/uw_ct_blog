<?php

/**
 * @file
 * uw_ct_blog.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_blog_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_blog-page-settings:admin/config/system/views_rename.
  $menu_links['menu-site-management_blog-page-settings:admin/config/system/views_rename'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/views_rename',
    'router_path' => 'admin/config/system/views_rename',
    'link_title' => 'Blog page settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_blog-page-settings:admin/config/system/views_rename',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags.
  $menu_links['menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_blog_tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Blog tags',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_blog-tags:admin/structure/taxonomy/uw_blog_tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog page settings');
  t('Blog tags');

  return $menu_links;
}
