<?php

/**
 * @file
 * uw_ct_blog.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_blog_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer views renaming'.
  $permissions['administer views renaming'] = array(
    'name' => 'administer views renaming',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_blog',
  );

  // Exported permission: 'create uw_blog content'.
  $permissions['create uw_blog content'] = array(
    'name' => 'create uw_blog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_blog content'.
  $permissions['delete any uw_blog content'] = array(
    'name' => 'delete any uw_blog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_blog content'.
  $permissions['delete own uw_blog content'] = array(
    'name' => 'delete own uw_blog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_blog_tags'.
  $permissions['delete terms in uw_blog_tags'] = array(
    'name' => 'delete terms in uw_blog_tags',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_blog content'.
  $permissions['edit any uw_blog content'] = array(
    'name' => 'edit any uw_blog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_blog content'.
  $permissions['edit own uw_blog content'] = array(
    'name' => 'edit own uw_blog content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_blog_tags'.
  $permissions['edit terms in uw_blog_tags'] = array(
    'name' => 'edit terms in uw_blog_tags',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_blog revision log entry'.
  $permissions['enter uw_blog revision log entry'] = array(
    'name' => 'enter uw_blog revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'merge uw_blog_tags terms'.
  $permissions['merge uw_blog_tags terms'] = array(
    'name' => 'merge uw_blog_tags terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_blog authored by option'.
  $permissions['override uw_blog authored by option'] = array(
    'name' => 'override uw_blog authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_blog authored on option'.
  $permissions['override uw_blog authored on option'] = array(
    'name' => 'override uw_blog authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_blog promote to front page option'.
  $permissions['override uw_blog promote to front page option'] = array(
    'name' => 'override uw_blog promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_blog published option'.
  $permissions['override uw_blog published option'] = array(
    'name' => 'override uw_blog published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_blog revision option'.
  $permissions['override uw_blog revision option'] = array(
    'name' => 'override uw_blog revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_blog sticky option'.
  $permissions['override uw_blog sticky option'] = array(
    'name' => 'override uw_blog sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
